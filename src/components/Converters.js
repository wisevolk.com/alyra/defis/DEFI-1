import React, { Component } from "react";
import {
    decimalToHex,
    hexToDecimal,
    littleEndianToHex,
    bitsToTarget,
    targetToDifficulty
} from "../utils/converters";

class Converters extends Component {

    state = {
        decimalToHexResult: "",
        hexToDecimalResult: "",
        littleEndianToHexResult: "",
        varintToDecimalResult: "",
        bitsToTargetResult: "",
        targetToDifficultyResult: ""
    };


    convertFromDecimalToHex = async (e) => {
        // console.log(decimalToHex(Number(document.getElementById("decimal").value)));
        this.setState({ decimalToHexResult: decimalToHex(Number(document.getElementById("decimal").value)) });
    };

    convertFromHexadecimalToDecimal = async (e) => {
        // console.log(hexToDecimal(document.getElementById("hexadecimal").value));
        this.setState({ hexToDecimalResult: hexToDecimal(document.getElementById("hexadecimal").value) });
    };

    convertFromLittleEndianToHexadecimal = async () => {
        this.setState({ littleEndianToHexResult: littleEndianToHex(document.getElementById("littleendian").value) });
    };

    convertFromVarintToDecimal = async (e) => {
        this.setState({ varintToDecimal: decimalToHex(Number(document.getElementById("varint").value)) });
        return undefined;
    };

    convertFromBitsToTarget = async (e) => {
        this.setState({ bitsToTarget: bitsToTarget(document.getElementById("bits").value) });
        return undefined;
    };

    convertFromTargetToDifficulty = async (e) => {
        this.setState({ targetToDifficulty: targetToDifficulty(document.getElementById("cible").value) });
    };

    render() {
        return (
            <div className="converters">
                <div className="input-group mb-3">
                    <input type="text" id="decimal" placeholder="Decimal to Hex"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" onClick={ this.convertFromDecimalToHex }>Convertir</button>
                    </div>
                    <p className="converted-value lead">{ this.state.decimalToHexResult }</p>
                </div>
                <div className="conversion-block input-group mb-3">
                    <input type="text" id="hexadecimal" placeholder="Hex to Decimal"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" onClick={ this.convertFromHexadecimalToDecimal }>Convertir
                        </button>
                    </div>
                    <p className="converted-value">{ this.state.hexToDecimalResult }</p>
                </div>
                <div className="conversion-block input-group mb-3">
                    <input type="text" id="littleendian" placeholder="Litte Endian to Hex"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary"
                                onClick={ this.convertFromLittleEndianToHexadecimal }>Convertir
                        </button>
                    </div>
                    <p className="converted-value">{ this.state.littleEndianToHexResult }</p>
                </div>
                <div className="conversion-block input-group mb-3">
                    <input type="text" id="varint" placeholder="Varint to Decimal"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" onClick={ this.convertFromVarintToDecimal }>Convertir
                        </button>
                    </div>
                    <p className="converted-value">{ this.state.varintToDecimal }</p>
                </div>
                <div className="conversion-block input-group mb-3">
                    <input type="text" id="bits" placeholder="Bits to Target"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" onClick={ this.convertFromBitsToTarget }>Convertir
                        </button>
                    </div>
                    <p className="converted-value">{ this.state.bitsToTarget }</p>
                </div>
                <div className="conversion-block input-group mb-3">
                    <input type="text" id="cible" placeholder="Target to Difficulty"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" onClick={ this.convertFromTargetToDifficulty }>Convertir
                        </button>
                    </div>
                    <p className="converted-value">{ this.state.targetToDifficulty }</p>
                </div>
            </div>
        );
    }
}

export default Converters;
