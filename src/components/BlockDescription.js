import React, { Component } from "react";
import block from "../json/block-test"
import client from "../utils/bitcoin-core-regtestnet";


class BlockDescription extends Component {

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12">
                        <h2>Description du bloc</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 text-left">
                        <p><b>Hash :</b> { block.hash }</p>
                        <p><b>version :</b>  { block.ver }</p>
                        <p><b>block précédent :</b> { block.prev_block }</p>
                        <p><b>Taille :</b> { block.size }</p>
                        <p><b>Transaction 1 :</b> { block.tx[0].hash }</p>
                        <p><b>Position :</b> { block.height }</p>
                        <button className="btn btn-info" onClick={ this.callNewBlock }>Appeler un nouveau block</button>
                    </div>
                </div>
            </div>
        );
    }

    callNewBlock = (e)=> {
        client.getBlockchainInfo()
            .then((help) => console.log(help))
            .catch(reason => console.log(reason));
    }
}

export default BlockDescription;
