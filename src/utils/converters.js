const regexDecimal = /[0-9]+/;
const regexHexa = /^([a-f]+\d*)|^(\d*[a-f]+)|^(\d+)/i;


const decimalToHex = (decimal) => {
    if ( !regexDecimal.test(decimal) ) {
        throw new Error("La valeur demandé n'est pas un entier de base 10");
    }
    const hexNumber = [];
    while ( decimal !== 0 ) {
        const reste = Math.trunc(decimal / 16);
        let hex = decimal - (reste * 16);
        if ( hex > 9 )
            hex = String.fromCharCode(hex + 55);

        //console.log(hex);
        hexNumber.push(hex);
        decimal = Math.trunc(decimal / 16);
    }
    return hexNumber.reverse().join("");
};

//Ne gère que les entiers
const hexToDecimal = (value) => {
    if ( regexHexa.test(value) ) {
        let decimal = 0;

        for ( let i = 0; i < value.length; i++ ) {
            const char = value[i].toUpperCase();
            if ( char.charCodeAt(0) <= 57 ) {
                decimal += Number(char) * Math.pow(16, value.length - i - 1);
            } else {
                decimal += Number(value[i].toUpperCase().charCodeAt(0) - 55) * Math.pow(16, (value.length - i - 1));
            }
            // console.log(`decimal : ${ decimal } ${ char.charCodeAt(0) }`);
        }
        return decimal;
    }
};

const littleEndianToHex = (val) => {
    try {
        if ( !regexHexa.test(val) )
            throw new Error("La valeur à convertir doit être au format hexadécimal");

        let valToConvert = (val.substr(0, 2) === "0x")
            ? val.substr(2) : val;
        if ( valToConvert.length % 2 !== 0 ) valToConvert = "0" + valToConvert;
        let valToConvertTab = [];
        for ( let i = 0; i < valToConvert.length; i += 2 ) {
            valToConvertTab.push(valToConvert.substr(i, 2));
        }
        console.log("0x" + valToConvertTab.reverse().join("").toUpperCase());
        return valToConvertTab;

    } catch (e) {
        console.log(`Error : ${ e.message }`);
    }
};

const bitsToTarget = (bits) => {
    const bitsex = "0x180696f4";
    const exponent = hexToDecimal(bits.substring(2,4));
    let target = bits.substring(4);
    for(let i = 0; i < (exponent * 2 - bits.length); i++){
        target += "0";
    }
    return target;
};

const targetToDifficulty = (target) => {
    const TARGET_MAX = (Math.pow(2, 16) - 1) * Math.pow(2, 208);
    return TARGET_MAX / Number(hexToDecimal(littleEndianToHex(target)));
};

module.exports = {
    decimalToHex,
    littleEndianToHex,
    hexToDecimal,
    bitsToTarget,
    targetToDifficulty
};
