const Client = require('bitcoin-core');
const client = new Client({
    network: 'testnet',
    username: 'bitcoin',
    headers: true,
    password: 'bitcoin',
    host:"localhost",
    port: 18332
});
// client.getInfo().then(([body, headers]) => console.log(body, headers));
/*
    LANCER LE FICHIER docker-compose.yml
 */


client.getBlockchainInfo()
    .then((help) => console.log(help))
    .catch(reason => console.log(reason));

// client.getBestBlockHash()
//     .then((help) => console.log(help))
//     .catch(reason => console.log(reason));

// client.getMiningInfo()
//     .then(info => console.log(info))
//     .catch(reason => console.log(reason));
//
// client.getBlock("000000000ae88125504e318a6833a89fa87b7ba4705a9ef8a3c7482be05dc034")
//     .then(info => console.log(info))
//     .catch(reason => console.log(reason));

// client.getTransaction("d5c286795b2cb4324888b0959a9744f47ac7359211970bc43ab6f6008b99652e")
//     .then(info => console.log(info))
//     .catch(reason => console.log(reason));

// client.getBalance()
//     .then(info => console.log(info))
//     .catch(reason => console.log(reason));
//
// client.listSinceBlock("0000000005cf174bfce670ffae27d81d1e79eee00127678d369a4a6fe022ec91")
//     .then(info => console.log("since ===> ", info))
//     .catch(reason => console.log(reason));


 //export default client;
