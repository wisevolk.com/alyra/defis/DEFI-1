import React, { Component } from 'react';
//import logo from './logo.s vg';
import bitcoin from "./imgs/BC_Logo.png"
import './css/App.css';
import Converters from "./components/Converters";
import BlockDescription from "./components/BlockDescription";

class App extends Component {
    render() {
        return (
            <div className="App container-fluid">
                <header className="App-header">
                    <img src={ bitcoin } className="App-logo" alt="logo"/>
                </header>
                <div className="row main">
                    <div className="col-lg-3 offset-lg-3">
                        <Converters />
                    </div>
                    <div className="col-lg-6">
                        <BlockDescription />
                    </div>
                </div>
            </div>
        )
    };
}

export default App;
