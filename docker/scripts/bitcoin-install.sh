#!/usr/bin/env bash

useradd -r bitcoin_user\
    && cd /usr/local \
    && git clone https://github.com/bitcoin/bitcoin.git \
    && cd ./bitcoin \
    && ./autogen.sh \
    && ./configure CPPFLAGS="-I/usr/local/BerkeleyDB.4.8/include -O2" LDFLAGS="-L/usr/local/BerkeleyDB.4.8/lib" \
    && make && make install \
    && mv /tmp/conf/bitcoin*.conf /etc/
