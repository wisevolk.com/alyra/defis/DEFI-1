#!/usr/bin/env bash

#if [ ! -e /etc/apt/sources.list.d/backports.list ]
#then
#    echo  "deb http://deb.debian.org/debian bulleyes-backports main" | tee -a /etc/apt/sources.list.d/backports.list
#fi

apt-get update && apt-get -y upgrade

apt-get install -y -q --no-upgrade \
    apt-utils \
    libzmq3-dev \
    doxygen \
    autoconf \
    libtool \
    libdb++-dev \
    pkg-config \
    libboost-all-dev \
    bsdmainutils \
    openssl \
    libssl-dev \
    dialog \
    nmap \
    iputils-ping \
    build-essential \
    linux-headers-amd64 \
    apt-transport-https \
    openssh-server \
    sasl2-bin \
    libnss3-dev \
    sudo \
    net-tools \
    iptables \
    locales \
    nano \
    vim \
    locate \
    openssl \
    cron \
    cron-apt \
    mailutils \
    curl \
    wget \
    sendmail-bin \
    sendmail \
    ntp \
    gnupg \
    unzip \
    git

echo "include(\`/etc/mail/tls/starttls.m4')dnl" >> /etc/mail/sendmail.mc \
  && echo "include(\`/etc/mail/tls/starttls.m4')dnl" >> /etc/mail/submit.mc \
  && yes y | sendmailconfig \
  && updatedb
