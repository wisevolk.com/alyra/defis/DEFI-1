#!/usr/bin/env bash

#cd /bitcoin/bdb-4.8

# Modifier dans le fichier ../dbinc/atomic.h
# __atomic_compare_exchange to __atomic_compare_exchange_db

wget http://download.oracle.com/berkeley-db/db-4.8.30.tar.gz \
    && tar xvzf db-4.8.30.tar.gz && rm -r db-4.8.30.tar.gz \
    && cd db-4.8.30/build_unix/ && ../dist/configure --enable-cxx \
    && sed -i "s/__atomic_compare_exchange/__atomic_compare_exchange_db/g" ../dbinc/atomic.h \
    && make && make install \
    && export BDB_INCLUDE_PATH="/usr/local/BerkeleyDB.4.8/include" \
    && export BDB_LIB_PATH="/usr/local/BerkeleyDB.4.8/lib" \
    && ln -s /usr/local/BerkeleyDB.4.8/lib/libdb-4.8.so /usr/lib/libdb-4.8.so \
    && ln -s /usr/local/BerkeleyDB.4.8/lib/libdb_cxx-4.8.so /usr/lib/libdb_cxx-4.8.so \
    && ldconfig \
    && updatedb
