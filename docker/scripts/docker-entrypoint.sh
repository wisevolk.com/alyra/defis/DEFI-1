#!/usr/bin/env bash

sed -i "s/rpcbind/rpcbind=$(hostname -I)/g" /etc/bitcoin*.conf \
  && bitcoind -conf=/etc/bitcoind.conf
  
