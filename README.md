## Défi #1 : Outil d’analyse bitcoin
Pour notre premier défi, notre objectif est de faire un outil d’analyse de la blockchain Bitcoin. L’outil pourra prendre la forme d’une page web ou d’un utilitaire en ligne de commandes. 

1. Dans un premier temps, on lui donne des informations issues de la blockchain, par exemple un nombre en little endian ou le champ Bits et il convertit les informations de façon explicite.
Voici quelques exemples:

    * Hexadécimal -> décimal
    * Décimal -> hexadécimal
    * Hexadécimal little endian -> hexadécimal
    * varInt -> décimal
    * Champ Bits -> Cible correspondante
    * Cible -> Difficulté
2. Dans un deuxième temps, on lui fournit un bloc (sous format JSON ou hexadécimal) et il affiche les informations contenues.
Par exemple, vous pouvez prendre le bloc suivant (attention, dans ce bloc la plupart des valeurs sont déjà converties).

3.  Enfin, de façon optionnelle, on peut récupérer les informations d’un noeud bitcoind directement en appelant l’interface JSON-RPC (https://en.bitcoin.it/wiki/API_reference_(JSON-RPC) ) et permettre la navigation. Défi additionnel, interpréter correctement la partie Script des transactions (opcodes et pile).
---
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Bitcoin-Core Docker registry

```
docker image pull registry.gitlab.com/wisevolk.com/alyra/defis/defi-1/bitcoin-core-testnet:1.0
```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
